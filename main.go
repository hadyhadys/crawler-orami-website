package main

import (
	"bufio"
	"encoding/csv"
	"os"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

// Product ...
type Product struct {
	Nama      string `json:"nama"`
	Deskripsi string `json:"deskripsi"`
	Detail    string `json:"detail"`
	Ukuran    string `json:"ukuran"`
	Harga     string `json:"harga"`
	Gambar    string `json:"gambar"`
}

const (
	// PageUtama ...
	PageUtama = "https://www.orami.co.id/c/fashion-dan-aksesoris?page="
	// FileLink File Link ...
	FileLink = "file/listlink.txt"
	// FileCSV File Result CSV
	FileCSV = "file/result.csv"
)

// Check ...
func Check(err error) {
	if err != nil {
		panic(err)
	}
}

// GetLinkProduct ...
func GetLinkProduct(c *colly.Collector, f *os.File, page string, counter int) (string, int) {
	// Find and visit all links
	flag := "false"
	links := ""
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		class := e.Attr("class")
		if class == "block-link-product-1" {
			link := e.Attr("href")
			// fmt.Println(counter, link)
			flag = "true"
			links = links + link + "\n"
			counter++
		}
	})

	c.Visit(PageUtama + page)

	w := bufio.NewWriter(f)
	_, err := w.WriteString(links)
	Check(err)
	w.Flush()

	return flag, counter
}

// Save Save CSV ...
func Save(data Product, writer *csv.Writer) {
	// Start save to csv

	err := writer.Write([]string{data.Nama, data.Harga, data.Gambar})
	Check(err)

	defer writer.Flush()
}

// SaveProductInfo ...
func SaveProductInfo(c *colly.Collector, writecsv *csv.Writer) {
	var product Product

	f, err := os.Open(FileLink)
	Check(err)

	scanner := bufio.NewScanner(f)
	counter := 0
	for scanner.Scan() {
		// Get Nama
		c.OnHTML("title", func(e *colly.HTMLElement) {
			product.Nama = e.Text
		})

		// Get Harga
		c.OnHTML("#product-price-option", func(e *colly.HTMLElement) {
			harga, _ := e.DOM.Find(".final-price").Attr("content")
			product.Harga = harga
		})

		// Get Image
		c.OnHTML("meta", func(e *colly.HTMLElement) {
			if e.Attr("property") == "og:image:url" {
				product.Gambar = e.Attr("content")
			}
		})

		// Get Description
		c.OnHTML("#highlight", func(e *colly.HTMLElement) {
			product.Deskripsi, err = e.DOM.Html()
			Check(err)
		})

		c.OnHTML("#description", func(e *colly.HTMLElement) {
			Detail, err := e.DOM.Html()
			product.Detail = strings.Replace(Detail, `<span class="showall">...selengkapnya</span>`, "", 1)
			Check(err)
		})

		c.Visit(scanner.Text())

		Save(product, writecsv)
		counter++
	}
}

func main() {
	c := colly.NewCollector(
		colly.MaxDepth(1),
	)

	counter := 1

	f, err := os.Create("file/listlink.txt")
	Check(err)

	defer f.Close()

	for i := 1; i < 100; i++ {
		page := strconv.Itoa(i)

		flag, c := GetLinkProduct(c, f, page, counter)

		if flag == "false" {
			break
		}
		counter = c
	}

	file, err := os.Create(FileCSV)
	Check(err)
	defer file.Close()

	writer := csv.NewWriter(file)
	Header := []string{"Nama", "Harga", "Gambar"}
	err = writer.Write(Header)
	Check(err)
	SaveProductInfo(c, writer)

}
